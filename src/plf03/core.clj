(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z #{-20 10 -3 5 -4 8 4 7 -10 12})))

(defn función-comp-2
  []
  (let [f (fn [xs] (into [] (remove pos? xs)))
        g (fn [xs] (reverse xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z [1 2 -5 4 -9 -6 -8])))
(defn función-comp-3
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (map-entry? xs))
        z (comp f g )]
    (z '(2 3 4 5))))
(defn función-comp-4
  []
  (let [f (fn [xs] (associative? xs))
        g (fn [xs] (map even? xs))
        h (fn [xs] ( vector zero? xs))
        z (comp f g h)]
    (z [100 200 10 20 300 400 0])))
(defn función-comp-5
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (boolean? xs))
        h (fn [xs] (decimal? xs))
        z (comp f g h)]
    (z '(:hola? adios :love amor)) ))
;;;;;;;;primer commit;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-comp-6
  []
  (let [f (fn [xs] (int? xs))
        g (fn [xs] (integer? xs))
        z (comp f g )]
    (z [1.2 1.0 :ab 100])))
(defn función-comp-7
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (map-entry? xs))
        h (fn [xs] (first xs))
        z (comp f g h)]
    (z '(2 3 4))))
(defn función-comp-8
  []
  (let [f (fn [xs] (repeat xs))
        g (fn [xs] (rest xs))
        z (comp f g )]
    (z ["a" "a" "b" "c" "d" "e"])))
(defn función-comp-9
  []
  (let [f (fn [xs] (filterv decimal? xs))
        g (fn [xs] (filter int? xs))
        h (fn [xs] (map integer? xs))
        z (comp f g h)]
    (z #{10.1 20 30 40.1})))

(defn función-comp-10
  []
  (let [f (fn [xs] (shuffle xs))
        g (fn [xs] (sort xs))
        h (fn [xs] (str xs))
        z (comp f g h)]
    (z [2 5 6 9 10 16 17 20 40 10 45 6 0 3 4])))
;;;;;;;;;;;;;segundo commit;;;;;;;;;;;;;;;;;
(defn función-comp-11
  []
  (let [f (fn [xs] (vals xs))
        ;g (fn [xs] (zero? xs))
        h (fn [xs] (zipmap [] xs))
        z(comp f h)]
    (z {:a  :b  :c  :d })))
(defn función-comp-12
  []
  (let [f (fn [xs] (keys xs))
        g (fn [xs] (seq xs))
        z(comp f g)]
    (z {:keys :and, :some :values})))
(defn función-comp-13
  []
  (let [f (fn [xs] (distinct xs))
        g (fn [xs] (map inc xs))
        h (fn [xs] (pmap inc xs))
        z (comp f g h)]
    (z [1 2 1 3 1 4 1 5])))
(defn función-comp-14
  []
  (let [f (fn [xs] (last xs))
        g (fn [xs] (next xs))
        h (fn [xs] (rest xs))
        z (comp f g h)]
    (z ["a" "b" "c" "d" "e"])))
(defn función-comp-15
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (conj xs))
        z (comp f g)]
    (z ["a" "b" "c"] )))
;;;;;;;;;;;;;tercer commit;;;;;;;;;;;
(defn función-comp-16
  []
  (let [f (fn [xs] (rational? xs))
        g (fn [xs] (ratio? xs))
        h (fn [xs] (double? xs))
        z (comp f g h)]
    (z [1.2 1 1.3 20 30])))
(defn función-comp-17
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (seqable? xs))
        z (comp f )]
    (z  [10 20 30])))
(defn función-comp-18
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (partition-by odd? xs))
        z (comp f g)]
    (z [1 1 1 2 2 3 3])))
(defn función-comp-19
  []
  (let [f (fn [xs] (char? xs))
        g (fn [xs] (filter some? xs))
        z(comp f g)]
   (z '(1 nil [] :a nil))))
(defn función-comp-20
  []
  (let [f (fn [xs] (false? xs))
        g (fn [xs] (filter even? xs))
        z (comp f g)]
    (z [20 30 40 50])))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)
;;;;;;;;Cuarto commit;;;;;
;Funciones de orden superior Complement
(defn función-complement-1
  []
  (let [f (fn [xs] (map even? xs))
        z(complement f)]
    (z '(1 2 3 4))))
(defn función-complement-2
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 20)))
(defn función-complement-3
  []
  (let [f (fn [x] (false? x))
        z (complement f)]
    (z true)))
(defn función-complement-4
  []
  (let [f (fn [x s] (compare x s))
        z (complement f)]
    (z 4 4)))
(defn función-complement-5
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z 0)))
;;;;;;Commit 5;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-complement-6
  []
  (let [f (fn [x] (decimal? x))
        z (complement f)]
    (z 1.0M)))
(defn función-complement-7
  []
  (let [f (fn [xs] (associative? xs))
        z(complement f)]
    (z {:becky "gameplays" :juega "Tombraider"})))
(defn función-complement-8
  []
  (let [f (fn [xs] (indexed? xs))
        z (complement f)]
    (z [10 20 30])))
(defn función-complement-9
  []
  (let [f (fn [xs] (map? xs))
        z (complement f)]
    (z '(1 2 3))))
(defn función-complement-10
  []
  (let [f (fn [xs] (string? xs))
        z (complement f)]
    (z "Rebeca paulina")))
;;;;;;;;;;;;;;;;;;;;;;;;;,,commit 6;;;;;;;;;;;;;;;;;;;;
(defn función-complement-11
  []
  (let [f (fn [xs] (not-empty xs))
        z(complement f)]
    (z [5])))
(defn función-complement-12
  []
  (let [f (fn [x] (str x))
        z (complement f)]
    (z 178)))
(defn función-complement-13
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z 10)))
(defn función-complement-14
  []
  (let [f (fn [x] (zero? x))
        z(complement f)]
    (z 0)))
(defn función-complement-15
  []
  (let [f (fn [x] (vector? x))
        z (complement f)]
    (z '(1 23 3 4))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;commit 7;;;;;;;;;;;
(defn función-complement-16
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z [1 2 3 4 5])))
(defn función-complement-17
  []
  (let [f (fn [x] (ident? x))
        z (complement f)]
    (z :xs)))
(defn función-complement-18
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z 45)))
(defn función-complement-19
  []
  (let [f (fn [x] (integer? x))
        z (complement f)]
    (z 2.0)))
(defn función-complement-20
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z 'x)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)
;;;;;;;;;;;;;;;;;;;commit 8;;;;;;;;;;;;;;;;;,
;Funciones de orden superior constantly
(defn función-constantly-1
  []
  (let [xs [:a :b :c]
        z (constantly xs)]
    (z [1 2 3 4 5])))
(defn función-constantly-2
  []
  (let [x 10
        z (constantly x)]
    (z 20 30 40)))
(defn función-constantly-3
  []
  (let [x true
        z (constantly x)]
    (z false)))
(defn función-constantly-4
  []
  (let [xs {:hola? :adios :burbuja :bellota}
        z (constantly xs)]
    (z {:hola :adios :bombon :rosa })))
(defn función-constantly-5
  []
  (let [xs [true false]
        z (constantly xs)]
    (z [true true])))
;;;;;;;;;;;;;;,commit 9;;;;;;;;;
(defn función-constantly-6
  []
  (let [xs '(:corazon :rey)
        z (constantly xs)]
    (z [20 10 20 10])))
(defn función-constantly-7
  []
  (let [xs #{100 200 300 400}
        z (constantly xs)]
    (z #{nil})))
(defn función-constantly-8
  []
  (let [x 40
        z (constantly x)]
    (z 40 60 80)))
(defn función-constantly-9
  []
  (let [xs #{:54 "caramelo" :7 "sandia"}
        z (constantly xs)]
    (z nil)))
(defn función-constantly-10
  []
  (let [xs [true]
            z (constantly xs)]
    (z true)))
;;;;;;;;;;;;;;;;commit 10;;;;;;;;;;;;;;;
(defn función-constantly-11
  []
  (let [xs 100
        z (constantly xs)]
    (z [100])))
(defn función-constantly-12
  []
  (let [xs [ 1]
        z (constantly xs)]
    (z false?)))
(defn función-constantly-13
  []
  (let [xs ["PaulinaCroft 29- gamertag"]
        z (constantly xs)]
    (z "Paulina 29- steam")))

(defn función-constantly-14
  []
  (let [x "No me gusta comer carne"
        z (constantly x)]
    (z ["no me gusta la carne de res"])))

(defn función-constantly-15
  []
  (let [xs {:a 1 :b 2}
        z (constantly xs)]
    (z {:a 1 :b 2})))
;;;;;;;;;;;;;;;;commit 11;;;;;;;;;;;;,
(defn función-constantly-16
  []
  (let [xs [3 < 2]
        z (constantly xs)]
    (z [3 > 2])))
(defn función-constantly-17
  []
  (let [x :x
        z (constantly x)]
    (z keyword? :x)))
(defn función-constantly-18
  []
  (let [xs {:a :b}
        z (constantly xs)]
    (z 934)))
(defn función-constantly-19
  []
  (let [xs [nil nil]
        z (constantly xs)]
    (z number? )))
(defn función-constantly-20
  []
  (let [xs [range 1 2]
        z (constantly xs)]
    (z seq?)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)
;;;;;;;;;;;;;;;;;commit 12;;;;;;;;;;;;;;;;;
;Funciones de orden superior every-pred
(defn función-every-pred-1
  []
  (let [f (fn [x] odd? x)
        g (fn [x] pos? x)
        z (every-pred f g)]
    (z 6)))

(defn función-every-pred-2
  []
  (let [f (fn [x] ( number? x))
        g (fn [x] (pos? x))
        z (every-pred f g) ]
    (z 11)))

(defn función-every-pred-3
  []
  (let [f (fn [xs] ( ratio? xs))
        g (fn [xs] (even? xs))
        z (every-pred f g)]
    (z [0 2/3 -2/3 1/4 ])))

(defn función-every-pred-4
  []
  (let [f (fn [x] ( float? x))
        g (fn [x] (pos? x))
        z (every-pred f)]
    (z 9999)))
(defn función-every-pred-5
  []
  (let [f (fn [x] ( int? x))
        g (fn [x] (integer? x))
        z (every-pred f g)]
    (z 45)))
(defn función-every-pred-6
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (int? xs))
        z (every-pred f g)]
    (z [2 4 6 7])))
;;;;;;;;;;;;;commit 13;;;;;;;;;;;;;;;;;;,
(defn función-every-pred-7
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (distinct xs))
        z (every-pred f g)]
    (z [2 4 6 7 2])))
(defn función-every-pred-8
  []
  (let [f (fn [xs] (empty xs))
        g (fn [xs] (conj xs))
        z (every-pred f g)]
    (z #{:a :b :c})))
(defn función-every-pred-9
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (flatten xs))
        z (every-pred f g)]
    (z [2 2 2 3 3 4 5 6 7 7 7 8 8 8])))
(defn función-every-pred-10
  []
  (let [f (fn [xs] (peek xs))
        g (fn [xs] (pop xs))
        z (every-pred f g)]
    (z [0 1 2 3 4 5 6 7 8 9])))
(defn función-every-pred-11
  []
  (let [f (fn [xs] (shuffle xs))
        g (fn [xs] (sort xs))
        z (every-pred f g)]
    (z [0 1 2 3 4 5 6 ])))
(defn función-every-pred-12
  []
  (let [f (fn [xs] (str xs))
        g (fn [xs] (vals xs))
        z (every-pred f g)]
    (z {:a :b  :c :d  :e :f })))
(defn función-every-pred-13
  []
  (let [f (fn [x] (nil? x))
        g (fn [x] (zero? x))
        z (every-pred f g)]
    (z 0)))
(defn función-every-pred-14
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (zipmap [] xs))
        z (every-pred f g)]
    (z [:a :e :i :o])))
(defn función-every-pred-15
  []
  (let [f (fn [xs] (boolean? xs))
        g (fn [xs] (char? xs))
        z (every-pred f g)]
    (z [\@ true])))
;;;;;;;;;;;;;;;;;;commit 14;;;;;;;;;;;;;;
(defn función-every-pred-16
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (str xs))
        h (fn [xs] (string? xs))
        z (every-pred f g h)]
    (z '(:hola? adios :love planeta))))
(defn función-every-pred-17
  []
  (let [f (fn [x] (ident? x))
        g (fn [x] (indexed? x))
        h (fn [xs] (keyword? xs))
        z (every-pred f g h)]
    (z :hola)))
(defn función-every-pred-18
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (vector? xs))
        h (fn [xs] (map? xs))
        z (every-pred f g h)]
    (z '(1 2 3 4 5 5))))
(defn función-every-pred-19
  []
  (let [f (fn [xs] (seqable? xs))
        g (fn [xs] (seq? xs))
        z (every-pred f g )]
    (z [4 5 6])))
(defn función-every-pred-20
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (set? xs))
        z (every-pred f g )]
    (z [11 12 13])))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)
;;;;;;;;;;;;;;;;;;;;;;;commit 15;;;;;;;;;;;;;;;;;;;
;;;;;;Funciones de orden superior fnil;;;;;;;;;;
(defn función-fnil-0
        []
        (let [f (fn [x y] (if (> x y) (+ x y) (- x y)))
              z (fnil f 7)]
          (z nil 6)))
(defn función-fnil-1
  []
  (let [f (fn [x y] (if (< x y) (inc x) (inc y)))
        z (fnil f 5)]
    (z nil 30)))

(defn función-fnil-2
  []
  (let [f (fn [x y] (if (> x y) (inc x) (inc y)))
        z (fnil f 8)]
    (z nil 20)))
(defn función-fnil-3
  []
  (let [f (fn [x y] (if (> x y) (/ x y) (* x y)))
        z (fnil f 10)]
    (z nil 15)))

(defn función-fnil-4
  []
  (let [f (fn [x y] (if (<= x y) (even? x ) (dec  y)))
        z (fnil f 20)]
    (z nil 10)))

(defn función-fnil-5
  []
  (let [f (fn [xs ys] (if (> (first xs) (first ys)) (+ (first xs) (last xs)) (- (first xs) (last xs))))
        z (fnil f [2 3])]
    (z nil [2 4])))

;;;;;;;;;;;;,commit 16;;;;;;;;;;;
(defn función-fnil-6
  []
  (let [f (fn [xs ys] (if (str xs ys) (concat xs ys) (conj xs ys)))
        z (fnil f '(a b c))]
    (z nil '(d e f))))

(defn función-fnil-7
  []
  (let [f (fn [xs ys] (if (list xs ys) (str xs ys) (vector xs ys)))
        z (fnil f '( a e i))]
    (z nil [1 2 3])))

(defn función-fnil-8
  []
  (let [f (fn [xs ys] (if (vector xs ys) (count xs) (list xs ys)))
        z (fnil f [10 20 30 40 50])]
    (z nil [1 2 3 4])))

(defn función-fnil-9
  []
  (let [f (fn [xs ys] (if (list xs) (peek xs) (pop ys)))
        z (fnil f '(a e i o u) )]
    (z nil [5 4 3 2 1])))

(defn función-fnil-10
  []
  (let [f (fn [xs ys] (if (vector xs) (pop xs) (pop ys)))
        z (fnil f ["h" "o" "l" "a"])]
    (z nil '(1 2 3))))

;;;;;;;;Commit 17;;;;;;;;;;;;;;;;;;;
(defn función-fnil-11
  []
  (let [f (fn [x y] (if (> x y) (sort x) (dec y)))
        z (fnil f 3 2 8)]
    (z nil 100)))

(defn función-fnil-12
  []
  (let [f (fn [xs ys] (if (string? xs) (repeat xs) (shuffle ys)))
        z (fnil f [:a :b :c])]
    (z nil [1 8 4 3 2])))

(defn función-fnil-13
  []
  (let [f (fn [xs ys] (if (map xs) (keyword :a) (str xs ys)))
        z (fnil f {:a :b })]
    (z nil [3 2 1 0])))

(defn función-fnil-14
  []
  (let [f (fn [xs ys] (if (map xs) (map? xs) (vector xs ys)))
        z (fnil f {:a :b})]
    (z nil [3 2 1 0 1 2 3])))

(defn función-fnil-15
  []
  (let [f (fn [x y] (if (int x) (pos? x) (number? y) ))
        z (fnil f 3)]
    (z nil 8)))

;;;;;;;;;;;Commit 18;;;;;;;;;;;;;;;;;;;,
(defn función-fnil-16
  []
  (let [f (fn [x] (if (number? x) (dec x) (str "falso")))
        z (fnil f nil)]
    (z (last (range 6)))))

(defn función-fnil-17
  []
  (let [f (fn [x y] (if (char? x) (str x) (str y )))
        z (fnil f \@)]
    (z nil "no es char" )))

(defn función-fnil-18
  []
  (let [f (fn [x y] (if (vector x) (keyword x) (str y)))
        z (fnil f [:a])]
    (z nil "es string")))

(defn función-fnil-19
  []
  (let [f (fn [xs ys] (if (zero? xs) (+ xs ys) (inc ys)))
        z (fnil f 0)]
    (z nil 2)))

(defn función-fnil-20
  []
  (let [f (fn [xs ys] (if (zero? xs) (dec xs) (- xs ys)))
        z (fnil f 10)]
    (z nil 4)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)
;;;;;;;;;;;;commit 19;;;;;;;;;;;;;
;;;;;;;Funciones de orden superior juxt;;;;;;;
(defn función-juxt-1
  []
  (let [f (fn [x] (first x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "Ya vamos a la mitad")))

(defn función-juxt-2
  []
  (let [f (fn [x] (first x))
        g (fn [x] (str x))
        z (juxt f g)]
    (z "becky")))
(defn función-juxt-3
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (associative? xs))
        z (juxt f g)]
    (z {:becky "gameplays" :juega "PUBG"})))
(defn función-juxt-4
  []
  (let [f (fn [x] (int x))
        g (fn [x] (boolean? x))
        z (juxt f g)]
    (z 34)))
(defn función-juxt-5
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (decimal? x))
        z (juxt f g)]
    (z 100)))
;;;;;;;;;;;;;;;;;;;;;;commit 20;;;;;;;;;
(defn función-juxt-6
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (pos? x))
        z (juxt f g)]
    (z 2.0)))
(defn función-juxt-7
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (list x))
        z (juxt f g)]
    (z :x)))
(defn función-juxt-8
  []
  (let [f (fn [x] (vector x))
        g (fn [x] (map? x))
        z (juxt f g)]
    (z 2000)))
(defn función-juxt-9
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (concat xs))
        z (juxt f g)]
    (z '(a b c d))))
(defn función-juxt-10
  []
  (let [f (fn [xs] (conj xs))
        g (fn [xs] (count xs))
        z (juxt f g)]
    (z  #{"a" "b" "c"})))
;;;;;;;;;commit 21;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-juxt-11
  []
  (let [f (fn [xs] (distinct? xs))
        g (fn [xs] (count xs))
        z (juxt f g)]
    (z ["a" "e" "i" "o" "u"])))
(defn función-juxt-12
  []
  (let [f (fn [xs] (empty xs))
        g (fn [xs] (true? xs))
        z (juxt f g)]
    (z '(a e i o u))))
(defn función-juxt-13
  []
  (let [f (fn [xs] (frequencies xs))
        g (fn [xs] (first xs))
        z (juxt f g)]
    (z ['a' 'a' 'a' 'a'])))
(defn función-juxt-14
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (count xs))
        z (juxt f g)]
    (z [0 1 2 3 4 5 6 7])))
(defn función-juxt-15
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (peek xs))
        z (juxt f g)]
    (z '(1 2 3 a b c))))

;;;;;;;;;;;;;;;commit 22;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-juxt-16
  []
  (let [f (fn [x] (ident? x))
        g (fn [x] (map-entry? x))
        z (juxt f g)]
    (z 465)))
(defn función-juxt-17
  []
  (let [f (fn [xs] (pop xs))
        g (fn [xs] (seq? xs))
        z (juxt f g)]
    (z [10 20 30])))
(defn función-juxt-18
  []
  (let [f (fn [xs] (disj xs))
        g (fn [xs] (count xs))
        z (juxt f g)]
    (z #{"a" "b" "c"})))
(defn función-juxt-19
  []
  (let [f (fn [xs] (empty? xs))
        g (fn [xs] (distinct? xs))
        z (juxt f g)]
    (z {:a 1 :b 2 :c 2})))
(defn función-juxt-20
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (number? xs))
        z (juxt f g)]
    (z {:a :hola :señor :locutor})))
(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)
;;;;;;;;;;;;;;;;;;;;;;;;commit 23;;;;;;;;;;;;;;;;;
;;; Funciones de orden superior Partial;;;;;;;;
(defn función-partial-1
  []
  (let [f (fn [xs ys] (str xs ys))
        z (partial f ["Es un vector"])]
    (z [0 1 2 3 4 5])))
(defn función-partial-2
  []
  (let [
        f (fn [xs ys] (vector xs ys))
        z (partial  f 5)]
    (z 10)))

(defn función-partial-3
  []
  (let [
        f (fn [xs ys] (range xs ys))
        z (partial f 3 )]
    (z 8)))
(defn función-partial-4
  []
  (let [f (fn [xs ys] (drop xs ys))
        z (partial f 3)]
    (z [10 20 30 40])))
(defn función-partial-5
  []
  (let [f (fn [xs ys] (drop-last xs ys))
        z (partial f 1)]
    (z [100 200 300 400])))
;;;;;;;;;;;;;;;;;Commit 24;;;;;;;;;;;;;;;;;;;
(defn función-partial-6
  []
  (let [f (fn [xs y] (hash-map xs y))
        z (partial f 5)]
    (z #{1 2 3 4})))
(defn función-partial-7
  []
  (let [f (fn [x y] (+ x y))
        z (partial f 5)]
    (z 10)))
(defn función-partial-8
  []
  (let [f (fn [xs] (list xs))
        z (partial f)]
    (z :b)))
(defn función-partial-9
  []
  (let [f (fn [xs xy xz] (vector xs xy xz))
        z (partial f [1 2] [2 4])]
    (z [2 3])))
(defn función-partial-10
  []
  (let [f (fn [xs xy xz xx]  (list xs xy xz xx ))
        z (partial f :a :b)]
    (z :c :d)))
;;;;;;;;;;Commit 25;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-partial-11
  []
  (let [f (fn [x y] (* x y))
        z (partial f 50)]
    (z 10)))
 (defn función-partial-12
   []
   (let [f (fn [x y] (< x y))
         z (partial f 100)]
     (z 200)))
(defn función-partial-13
  []
  (let [f (fn [x y] (> x y))
        z (partial  f 500)]
    (z 499)))
(defn función-partial-14
  []
  (let [f (fn [x y] (<= x y))
        z (partial f 1000)]
    (z 1000)))
(defn función-partial-15
  []
  (let [f (fn [x y] (>= x y))
        z (partial f 2000)]
    (z 3000)))
;;;;;;;;;;Commit 26;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-partial-16
  []
  (let [f (fn [x y] (- x y))
        z (partial f 2000)]
    (z 100)))
(defn función-partial-17
  []
  (let [f (fn [x y] (str x y))
        z (partial f "Cuando pienso en ti")]
    (z "tu recuerdo hiere")))
(defn función-partial-18
  []
  (let [f (fn [x ys] (drop-while x ys))
        z (partial f neg?)]
    (z [-12 12 12])))
(defn función-partial-19
  []
  (let [f (fn [x ys] (group-by x ys))
        z (partial f set)]
    (z ["meat" "mat" "team" "mate" "eat" "tea"])))
(defn función-partial-20
  []
  (let [f (fn [xs ys ] (mapv xs ys ))
        z (partial f inc)]
    (z [1 2 3 4 5])))
(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)
;;;;;;;;;;;;;;;;;;;Commit 27;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;Funciones de primer orden some-fn;;;;;;;;;;
(defn función-some-fn-1
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (number? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z 2 4 6)))
(defn función-some-fn-2
  []
  (let [f (fn [xs] (associative? xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (map? xs))
        z (some-fn f g h)]
    (z {:becky "gameplays" :juega "Conker's bad fur day"})))
(defn función-some-fn-3
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (char? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z \a)))
(defn función-some-fn-4
  []
  (let [f (fn [x] (flatten x))
        g (fn [x] (first x))
        h (fn [x] (int x))
        z (some-fn f g h)]
    (z 5)))
(defn función-some-fn-5
  []
  (let [f (fn [xs] (frequencies xs))
        g (fn [xs] (first xs))
        h (fn [xs] (int xs))
        z (some-fn f g h)]
    (z [2 2 2 3 3 4 5 6 7 7 7 8 8 8])))
;;;;;;;;;;;;Commit 28;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-some-fn-6
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (even? x))
        h (fn [x] (inc x))
        z (some-fn f g h)]
    (z 40)))
(defn función-some-fn-7
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (boolean x))
        h (fn [x] (int x))
        z (some-fn f g h)]
    (z 50)))
(defn función-some-fn-8
  []
  (let [f (fn [xs] (peek xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (inc xs))
        z (some-fn f g h)]
    (z '(a b c))))
(defn función-some-fn-9
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (dec x))
        h (fn [x] (float x))
        z (some-fn f g h)]
    (z 1)))
(defn función-some-fn-10
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (decimal? x))
        h (fn [x] (number? x))
        z (some-fn f g h)]
    (z 100)))
;;;;;;;;;;;;;Commit 29;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-some-fn-11
  []
  (let [f (fn [x] (range x))
        g (fn [x] (distinct? x))
        h (fn [x] (distinct x))
        z (some-fn f g h)]
    (z 10)))
(defn función-some-fn-12
  []
  (let [f (fn [xs] (rest xs))
        g (fn [xs] (map? xs))
        h (fn [xs] (list? xs))
        z (some-fn f g h)]
    (z ["a" "b" "c" "d" "e"])))
(defn función-some-fn-13
  []
  (let [f (fn [xs] (shuffle xs))
        g (fn [xs] (frequencies xs))
        h (fn [xs] (list? xs))
        z (some-fn f g h)]
    (z [1 8 4 2 3])))
(defn función-some-fn-14
  []
  (let [f (fn [xs] (sort xs))
        g (fn [xs] (mapcat xs))
        h (fn [xs] (odd? xs))
        z (some-fn f g h)]
    (z [2 5 6 9 10 16 17 20 40 10 45 6 0 3 4])))
(defn función-some-fn-15
  []
  (let [f (fn [x] (str x))
        g (fn [x] (frequencies x))
        h (fn [x] (list? x))
        z (some-fn f g h)]
    (z "Ultimas funciones")))
;;;;;;;;;;;;;;;;;;;Commit 30;;;;;;;;;;;;;;;;;;;;;;;;
(defn función-some-fn-16
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (true? xs))
        h (fn [xs] (number? xs))
        z (some-fn f g h)]
    (z [12 13 14 15])))
(defn función-some-fn-17
  []
  (let [f (fn [x] (keyword x))
        g (fn [x] (shuffle x))
        h (fn [x] (list? x))
        z (some-fn f g h)]
    (z :a :b)))
(defn función-some-fn-18
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (true? xs))
        h (fn [xs] (even? xs))
        z (some-fn f g h)]
    (z [12 13 14 15 16 17 18 19 20])))
(defn función-some-fn-19
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (false? xs))
        h (fn [xs] (conj xs))
        z (some-fn f g h)]
    (z [30 31 32 33 34 35])))
(defn función-some-fn-20
  []
  (let [f (fn [xs] (drop-last xs))
        g (fn [xs] (drop xs))
        h (fn [xs] (drop-while xs))
        z (some-fn f g h)]
    (z {:a 1 :b 2 :c 3 :d 4})))
(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)
;;;;;;;;;;;;;;Commit Final;;;;;;;;;;;;;;;;;;;;;;;;;;;;